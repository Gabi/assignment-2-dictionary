extern string_equals
global find_word

find_word:
    push rsi      ; store callee-saved register rsi
    push r12      ; store callee-saved register r12
    mov r12, rsi  ; store matching string address in r12

    .loop:
        add r12, 8           ; skipping value of pointer of previous element
        mov rsi, r12         ; now here is an address of dictionary value
        call string_equals   ; compare with rdi string, result in rax

        test rax, rax        ; check if strings are equal
        jnz .equals          ; if equal, jump to .equals

        sub r12, 8           ; revert to the value of pointer of previous element
        mov rax, [r12]       ; switch to next element
        test rax, rax        ; check if end of dictionary is reached
        jz .not_equals       ; if end, jump to .not_equals
        mov r12, rax         ; update r12 to point to next element
        jmp .loop            ; continue looping

    .equals:
        mov rax, rsi         ; return dictionary address of value
        jmp .return

    .not_equals:
        xor rax, rax         ; set rax to 0 for not found

    .return:
        pop r12              ; restore callee-saved register r12
        pop rsi              ; restore callee-saved register rsi
        ret                  ; return from function
