%include "colon.inc"

section .rodata

colon "first_w", first_w
db "first_word_explanation", 0

colon "second_w", second_w
db "second_word_explanation", 0

colon "third_w", third_w
db "third_word_explanation", 0

colon "a b c", abc
db "abc", 0

colon "three fucking words", tfw
db "eqjkwnjkqwndjqwndqwjqwndqwd  ", 0

colon "Gabitrue", Gabitrue
db "Gabitrue", 0

colon "piko", piko
db "piko", 0

colon "test", test
db "test", 0
